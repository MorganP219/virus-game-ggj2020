﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIMovement : MonoBehaviour
{
    [Header("Type Of AI")]
    public VirusAIType type;
    [Header("Speed Of The Object")]
    public float speedNow = 0.01f;
    private float timeNow;
    private GameObject playerController;
    // Start is called before the first frame update
    void Start()
    {
        playerController = GameObject.Find("PlayerController");
    }

    // Update is called once per frame
    void Update()
    {

        timeNow += Time.deltaTime;
        switch (type)
        {
            case VirusAIType.WORM:
                //Move To player;
          //      print(timeNow);
                if (timeNow > 0.0)
                {
                //    print("A");
                    var lookPos = playerController.transform.position - transform.position;
                    lookPos.y = 0;
                    var rotation = Quaternion.LookRotation(lookPos);
                   transform.rotation = Quaternion.Slerp(transform.rotation, rotation,  1f);

                 //  transform.position = Vector3.MoveTowards(transform.position, playerController.transform.position, speedNow);
                    timeNow = 0;
                }
                
                break;
            
            case VirusAIType.ROCKINGCHAIRHORSE:
                if (timeNow > 0.0)
                {
                    //print("B");
                    var lookPos = playerController.transform.position - transform.position;
                    lookPos.y = 0;
                    var rotation = Quaternion.LookRotation(lookPos);
                    
                    transform.rotation = Quaternion.Slerp(transform.rotation, rotation,  1f);
                    var y = playerController.transform.position.y;
                    
                    transform.position = Vector3.MoveTowards(transform.position, new Vector3(playerController.transform.position.x, y, playerController.transform.position.z), speedNow);
                  //  print(transform.position);
                    timeNow = 0;
                }
                break;
            case VirusAIType.ANT:
                if (timeNow > 0.0)
                {
                  //  print("C");
                    var lookPos = playerController.transform.position - transform.position;
                    lookPos.y = 0;
                    var rotation = Quaternion.LookRotation(lookPos);
                    transform.rotation = Quaternion.Slerp(transform.rotation, rotation,  1f);

                    transform.position = Vector3.MoveTowards(transform.position, playerController.transform.position, speedNow);
                    timeNow = 0;
                }
                break;
            case VirusAIType.SLIME:
                if (timeNow > 0.0)
                {
                //    print("C");
                    var lookPos = playerController.transform.position - transform.position;
                    lookPos.y = 0;
                    var rotation = Quaternion.LookRotation(lookPos);
                    transform.rotation = Quaternion.Slerp(transform.rotation, rotation,  1f);

                    transform.position = Vector3.MoveTowards(transform.position, playerController.transform.position, speedNow);
                    timeNow = 0;
                }
                break;
            
        }
    }
    
    public enum VirusAIType
    {
        WORM, ROCKINGCHAIRHORSE, ANT, SLIME
    }
}
