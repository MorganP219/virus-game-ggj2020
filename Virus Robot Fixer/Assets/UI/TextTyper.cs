﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class TextTyper : MonoBehaviour
{
    public TextMeshProUGUI UIText;

    private String textToSend;
    // Start is called before the first frame update
    void Start()
    {
        textToSend = UIText.text;
        UIText.text = "";
        StartCoroutine(startTyping());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public IEnumerator startTyping()
    {
        foreach (var charNow in textToSend)
        {
            UIText.text = UIText.text + charNow;
            yield return new WaitForSeconds(0.1f);
        }
    }
}
