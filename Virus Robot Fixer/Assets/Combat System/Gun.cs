﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Gun : MonoBehaviour
{
    public GameObject particleSyslem;
    private int bulletIDS = 0;
    private List<GameObject> allBullets;
    public Ray ray;
    public RaycastHit hit;
    public RaycastHit[] hits;
    
    private GameObject interactionMgr;
    // Start is called before the first frame update
    void Start()
    {
       particleSyslem = GameObject.Find("PlayerParticleSystem");
       interactionMgr = GameObject.Find("GameManager");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Shoot();
        }
    }
    

    public void Shoot()
    {
        particleSyslem.GetComponent<ParticleSystem>().Play();
        ray = new Ray(transform.position, transform.forward);
        hits = Physics.RaycastAll(transform.position, transform.forward, 100f);
        for (int i = 0; i < hits.Length; i++)
        {
            RaycastHit hit = hits[i];
            print(hit.collider.gameObject.tag);
            if (hit.collider.gameObject)
            {
                Debug.DrawLine(ray.origin, ray.direction * 1000, Color.green);
            }
            else
            {
                Debug.DrawLine(ray.origin, ray.direction * 1000, Color.red);
            }

            print(hit.collider.gameObject.name);
            if (hit.collider.gameObject.CompareTag("Killable"))
            {
                if (hit.collider.name == "Cablebox")
                {
                    
                    hit.collider.GetComponent<Interactable>().hasAttackedBox();
                }
            print(hit.collider.name);
                if (hit.collider.name == "HitMeToWin")
                {
                    hit.collider.GetComponent<Interactable>().hasHitToWin();
                }
                else
                {
                    hit.collider.gameObject.GetComponent<AICombat>().Kill();
                }
                 
                 
            }
        }
        /*
         *  if (Physics.Raycast(ray, out hit, 1000))
         {
             
             if (hit.collider.gameObject)
             {
                 Debug.DrawLine(ray.origin, ray.direction * 1000, Color.green);
             }
             else
             {
                 Debug.DrawLine(ray.origin, ray.direction * 1000, Color.red);
             }
             print(hit.collider.gameObject.name);
             if (hit.collider.gameObject.CompareTag("Killable"))
             {
                 if (hit.collider.name == "Cablebox")
                 {
                     interactionMgr.GetComponent<Interactable>().hasAttackedBox();
                 }
                 else
                 {
                     hit.collider.gameObject.GetComponent<AICombat>().Kill();
                 }
                 
                 
             }
         }
         */
    }

    public void GetInfoFromBullet(GameObject bulletNow, int info)
    {
        if (info == 0)
        {
            allBullets.Remove(bulletNow);
        }
    }
}
