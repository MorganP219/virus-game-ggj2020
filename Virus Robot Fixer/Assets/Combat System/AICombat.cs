﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AICombat : MonoBehaviour
{
    public AIMovement.VirusAIType type;
    private bool isDead = false;
    private bool hasPinged = false;
    [Header("Slime ONLY")]
    public float seperationMaxRange = 5.0f;
    public GameObject Slime;
        public bool slimeHasSplit = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void Kill()
    {
        print("KILLED: " + type);
        isDead = true;
        switch (type)
        {
            case AIMovement.VirusAIType.SLIME:
                if (!slimeHasSplit)
                {
                    for (int i = 0; i < 4; i++)
                    {
                        GameObject objTemp = Instantiate(Slime, transform.position, Quaternion.identity);
                        objTemp.transform.localScale = new Vector3(0.5f,0.5f,0.5f);
                        objTemp.transform.position = new Vector3(Random.Range(transform.position.x - seperationMaxRange,transform.position.x + seperationMaxRange), objTemp.transform.position.y, Random.Range(transform.position.z - seperationMaxRange,transform.position.z + seperationMaxRange));
                        objTemp.GetComponent<AICombat>().slimeHasSplit = true;
                    }

                    slimeHasSplit = true;
                }
                Destroy(this.gameObject);
                break;
            default:
                //Do Stuffs Here
                Destroy(this.gameObject);
                break;
        }
    }
}
