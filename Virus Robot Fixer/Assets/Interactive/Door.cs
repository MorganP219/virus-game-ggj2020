﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    public enum DoorTypes
    {
        SLIDE, DOWN
    }

    public enum Direction
    {
        LEFT, RIGHT
    }
    [Header("Type Of Door")]
    public DoorTypes type;

    public Direction dir;
    // Start is called before the first frame update
    void Start()
    {
       // StartCoroutine(Wait(5));
    }
    public IEnumerator Wait(float Time)
    {
        yield return new WaitForSeconds(Time);
        StartCoroutine(openDoor());
    }
    

    public IEnumerator WaitToDestroy(float Time)
    {
        yield return new WaitForSeconds(Time);
        Destroy(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public IEnumerator openDoor()
    {
        StartCoroutine(WaitToDestroy(8));
        switch (type)
        {
            case DoorTypes.SLIDE:
             //   print(transform.position.z);
                while (transform.position.z  <= transform.position.z + 2f)
                {
                    if (dir == Direction.RIGHT)
                    {
                        transform.Translate(Vector3.up * Time.deltaTime * 1.01f);
                    }
                    else
                    {
                        transform.Translate(Vector3.down * Time.deltaTime * 1.01f);
                    }
                    yield return null;
                    if (transform.position.z >= transform.position.z + 5f)
                    {
                        Destroy(gameObject);
                    }
                }
                break;
            case DoorTypes.DOWN:
                while (transform.position.y >= transform.position.y +  -1.1f)
                {
                    transform.Translate(Vector3.back * Time.deltaTime * 2);
                    yield return null;
                    if (transform.position.z <= transform.position.z + -1.1f)
                    {
                        Destroy(gameObject);
                    }
                }
                
               
                break;
        }
    }
}
