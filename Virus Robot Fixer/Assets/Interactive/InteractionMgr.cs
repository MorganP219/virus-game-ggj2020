﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;
using UnityEngine.UI;
using Image = UnityEngine.UI.Image;

public class InteractionMgr : MonoBehaviour
{
    public bool buttonOne;
    public bool buttonTwo;
    public bool buttonThree;
    public bool LeverOne;
    public bool LeverTwo;
    public bool LeverThree;
    public bool BoxOne;
    public bool BoxTwo;
    public bool BoxThree;
    private SCENES sceneName;
    private GameObject DoorInscene;
    private GameObject BarDoors;
    private Door door;
    private Door barDoor;
    public GameObject panel;
    private bool wait = false;
    public void sceneTrigger()
    {
        
        switch (sceneName)
        {
            case SCENES.LVL11:
                if (buttonOne)
                {
                    StartCoroutine(door.openDoor());

                }
                break;
            case SCENES.LVL12:
                if (BoxOne)
                {
                    StartCoroutine(door.openDoor());
                }
                break;
            case SCENES.LVL13:
                if (buttonOne)
                {
                    StartCoroutine(door.openDoor());
                }

                if (LeverOne)
                {
                    StartCoroutine(barDoor.openDoor());
                }
                break;
            case SCENES.LVL21:
                if (buttonOne)
                {
                    StartCoroutine(door.openDoor());
                }
                break;
            case SCENES.LVL22:
                if (LeverOne && LeverTwo && LeverThree)
                {
                    StartCoroutine(door.openDoor());
                }
                break;
            case SCENES.LVL23:
                if (buttonOne)
                {
                    StartCoroutine(door.openDoor());
                }
                break;
            case SCENES.FINAL:
                if (buttonOne && buttonTwo)
                {
                    StartCoroutine(door.openDoor());
                }
                break;
        }
    }

    private void Start()
    {
        panel = GameObject.Find("Panel");
        Scene sceneNow = SceneManager.GetActiveScene();
        switch (sceneNow.name)
        {
            case "Level 1.1":
                sceneName = SCENES.LVL11;
                break;
            case "Level 1.2":
                sceneName = SCENES.LVL12;
                break;
            case "Level 1.3":
                sceneName = SCENES.LVL13;
                break;
            case "Level 2.1":
                sceneName = SCENES.LVL21;
                break;
            case "Level 2.2":
                sceneName = SCENES.LVL22;
                break;
            case "Level 2.3":
                sceneName = SCENES.LVL23;
                break;
            case "FinalLevel":
                sceneName = SCENES.FINAL;
                break;
        }
        DoorInscene = GameObject.Find("Door");
        door = DoorInscene.GetComponent<Door>();
        BarDoors = GameObject.Find("BarDoor");
        if (BarDoors)
        {
            barDoor = BarDoors.GetComponent<Door>();
        }

        StartCoroutine(fader(true));

    }

    public IEnumerator fader(bool isShow)
    {
        if (isShow)
        {
            while (panel.GetComponent<Image>().color.a > 0) 

            {
                print(panel.GetComponent<Image>().color.a);
                panel.GetComponent<Image>().color = new Color(0,0,0,panel.GetComponent<Image>().color.a - 0.01f);
                yield return new WaitForSeconds(0.01f);
            }
            
        }
        else
        {
            while (panel.GetComponent<Image>().color.a < 1) 

            {
                print(panel.GetComponent<Image>().color.a);
                panel.GetComponent<Image>().color = new Color(0,0,0,panel.GetComponent<Image>().color.a + 0.01f);
                yield return new WaitForSeconds(0.01f);
            }
            print(wait);

            wait = true;
        }
    }

    public IEnumerator hasHitSwitch()
    {
        print("Started");
        StartCoroutine(fader(false));
        while (!wait)
        {
         print("Apple");   
            yield return null;
        }
        switch (sceneName)
        {
            case SCENES.LVL11:
                SceneManager.LoadScene("Level 1.2");
                break;
            case SCENES.LVL12:
                SceneManager.LoadScene("Level 1.3");
                break;
            case SCENES.LVL13:
                SceneManager.LoadScene("Level 2.1");

                break;
            case SCENES.LVL21:
                SceneManager.LoadScene("Level 2.2");
                break;
            case SCENES.LVL22:
                print("A");
                SceneManager.LoadScene("Level 2.3");
                break;
            case SCENES.LVL23:
                SceneManager.LoadScene("FinalLevel");
                break;
            case SCENES.FINAL:

                break;
        }
    }


    public void OnTrigger(int id, bool isPressed, InterableType type)
    {
        print("I AM ID: " + id + " I HAVE A VALUE OF: " + isPressed + " WITH A TYPE OF " + type.ToString());
        switch (type)
        {
            case InterableType.LEVER:
                switch (id)
                {
                    case 0:
                        LeverOne = isPressed;
                        break;
                    case 1:
                        LeverTwo = isPressed;
                        break;
                    case 2:
                        LeverThree = isPressed;
                        break;
                }
                break;
            case InterableType.BUTTON:
                switch (id)
                {
                    case 0:
                        buttonOne = isPressed;
                        break;
                    case 1:
                        buttonTwo = isPressed;
                        break;
                    case 2:
                        buttonThree = isPressed;
                        break;
                }
                break;
            case InterableType.BOX:
                switch (id)
                {
                    case 0:
                        BoxOne = isPressed;
                        break;
                    case 1:
                        BoxTwo = isPressed;
                        break;
                    case 2:
                        BoxThree = isPressed;
                        break;
                }
                break;
        }    
        sceneTrigger();
    }
    
    
    public enum SCENES
    {
        LVL11, LVL12, LVL13, LVL21, LVL22, LVL23, FINAL
    }
    public enum InterableType
    {
        LEVER, BUTTON, BOX
    }

    public void setOldScene(Scene sc)
    {
        SceneManager.UnloadSceneAsync(sc);
    }
}
