﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour
{
    private GameObject gameManager;
    public InteractionMgr.InterableType type;
    public bool isPressed;

    private void Start()
    {
        gameManager = GameObject.Find("GameManager");
    }

    [Tooltip("USE THIS TO HAVE ACTIONS")] 
    public int id;
    public void Trigger() 
    {
     print("I Triggered"); 
     gameManager = GameObject.Find("GameManager");
     if (type == InteractionMgr.InterableType.BUTTON)
     {
         StartCoroutine(buttonPress(5));
     }
     else if (type == InteractionMgr.InterableType.LEVER)
     {
         isPressed = true;
         gameManager.GetComponent<InteractionMgr>().OnTrigger(id, isPressed, type);
     }

     
    }

    public IEnumerator buttonPress(float time)
    {
        isPressed = true;
        gameManager.GetComponent<InteractionMgr>().OnTrigger(id, isPressed, type);
        yield return new WaitForSeconds(time);
        gameManager.GetComponent<InteractionMgr>().OnTrigger(id, isPressed, type);
    }

    public void hasAttackedBox()
    {
        print("A");
        gameManager.GetComponent<InteractionMgr>().OnTrigger(id, true, InteractionMgr.InterableType.BOX);
    }

    public void hasHitToWin()
    {
        StartCoroutine(gameManager.GetComponent<InteractionMgr>().hasHitSwitch());
    }
}
