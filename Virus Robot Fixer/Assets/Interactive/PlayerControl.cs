﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    [Header(("Raycast Distance"))]
    public float maxDistance = 1000;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //
        RaycastHit hit;

        Ray ray = new Ray(GetComponent<Camera>().transform.position, GetComponent<Camera>().transform.forward);
        Debug.DrawLine(ray.origin, ray.direction * maxDistance, Color.green);
        if (Physics.Raycast(ray, out hit, maxDistance))
        {
            
            if (hit.collider.gameObject.CompareTag("Hittable"))
            {
                hit.collider.gameObject.GetComponent<Interactable>().Trigger();
                
            }
        }

    }
}
